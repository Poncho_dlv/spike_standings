#!/usr/bin/env python
# -*- coding: utf-8 -*-

from spike_database.Competitions import Competitions
from spike_database.Leagues import Leagues
from spike_database.ResourcesRequest import ResourcesRequest
from spike_requester.CyanideApi import CyanideApi
from spike_standings.Exception import CompetitionNotFoundError


class BloodBowl2Standing:

    @staticmethod
    def compute_standing(competition_id: int, platform_id: int):
        competition_db = Competitions()
        league_db = Leagues()
        rsrc_db = ResourcesRequest()
        standing = None
        point_system = None

        competition_data = competition_db.get_competition_data(competition_id, platform_id)
        if competition_data is not None:
            league_id = competition_data.get("league_id")
            if league_id is not None:
                league_data = league_db.get_league_data(league_id, platform_id)
                if league_data is not None:
                    league_name = league_data.get("name")
                    competition_name = competition_data.get("name")
                    platform = rsrc_db.get_platform_label(platform_id)
                    limit = competition_data.get("round", 10) * competition_data.get("teams_max", 20)
                    standing = []
                    point_system = None

                    data = CyanideApi.get_ladder(league_name=league_name, competition_name=competition_name, ladder_size=limit, platform=platform)
                    if data is not None and data.get("ranking") is not None:
                        for team in data["ranking"]:
                            if team.get("coach", {}).get("id") != 0:
                                if team.get("coach", {}).get("id") == 0:
                                    team["coach"]["name"] = "AI"

                                wdl = team.get("team", {}).get("w/d/l", "0/0/0")
                                win = int(wdl.split("/")[0])
                                draw = int(wdl.split("/")[1])
                                loss = int(wdl.split("/")[2])
                                games_played = win + draw + loss

                                standing.append({
                                    "rank": team["team"]["rank"],
                                    "coach_name": team["coach"]["name"],
                                    "team_name": team["team"]["name"],
                                    "logo": team["team"]["logo"],
                                    "race": team["team"]["race"],
                                    "points": team["team"]["score"],
                                    "win": win,
                                    "draw": draw,
                                    "loss": loss,
                                    "games_played": games_played,
                                    "value": team["team"]["tv"]
                                })
                        competition_db.set_standing(competition_id, platform_id, standing, "blood_bowl_2_standing", "default_bb2")
                    else:
                        raise CompetitionNotFoundError(competition_name, league_name, platform)
        return standing, point_system

    @staticmethod
    def get_standing(competition_id: int, platform_id: int):
        competition_db = Competitions()
        cmp_standing = competition_db.get_standing(competition_id, platform_id)

        if cmp_standing is None:
            standing, point_system = BloodBowl2Standing.compute_standing(competition_id, platform_id)
        else:
            point_system = cmp_standing["point_system"]
            standing = cmp_standing["standing"]
        return standing, point_system, "blood_bowl_2_standing"
