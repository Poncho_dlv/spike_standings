
class CompetitionNotFoundError(Exception):
    def __init__(self, competition_name, league_name=None, platform=None):
        if platform is None and league_name is None:
            self.message = "Competition **{}** not found!".format(competition_name)
        elif league_name is None and platform is not None:
            self.message = "Competition **{}** not found on **{}**!".format(competition_name, platform)
        else:
            self.message = "Competition **{}** not found in **{}** - **{}**!".format(competition_name, league_name, platform)


class GoblinSpyUnreachable(Exception):
    def __init__(self):
        self.message = "Goblin Spy unreachable."


class InvalidCompetitionFormat(Exception):
    def __init__(self):
        self.message = "Invalid competition format."
