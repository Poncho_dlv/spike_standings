#!/usr/bin/env python
# -*- coding: utf-8 -*-

from spike_database.Competitions import Competitions
from spike_database.CustomCompetitionTeam import CustomCompetitionTeam

EUROPEN_ID = 158165


class SpikeEurOpen:

    @staticmethod
    def compute_standing():
        standing = []
        competition_db = Competitions()
        custom_competition_team_db = CustomCompetitionTeam()
        wales = competition_db.get_competition_data(163358, 1)
        portugal = competition_db.get_competition_data(163361, 1)
        sweden = competition_db.get_competition_data(163363, 1)
        europen_team = custom_competition_team_db.get_teams(EUROPEN_ID, 1)
        point_system = "2-1-0,WIN,TDD+CASD"

        for team in europen_team:
            team_data = {
                "team_name": team.get("name"),
                "coach_names": [],
                "races": [],
                "points": 0,
                "win": 0,
                "draw": 0,
                "loss": 0,
                "games_played": 0,
                "td_for": 0,
                "td_against": 0,
                "tdd": 0,
                "casualties_inflicted": 0,
                "casualties_sustained": 0,
                "casualties_difference": 0,
                "kill": 0,
                "tdd_casd": 0,
            }
            members = team.get("members")
            for member in members:
                match = next((item for item in wales.get("standing", [{}]) if item.get("coach_id") == member.get("coach_id")), None)
                if match is None:
                    match = next((item for item in portugal.get("standing", [{}]) if item.get("coach_id") == member.get("coach_id")), None)
                if match is None:
                    match = next((item for item in sweden.get("standing", [{}]) if item.get("coach_id") == member.get("coach_id")), None)
                if match is None:
                    match = {}

                team_data["coach_names"].append(member.get("coach_name"))
                if match.get("race") is not None:
                    team_data["races"].append(match.get("race"))
                team_data["points"] += match.get("points", 0)
                team_data["win"] += match.get("win", 0)
                team_data["draw"] += match.get("draw", 0)
                team_data["loss"] += match.get("loss", 0)
                team_data["games_played"] += match.get("games_played", 0)
                team_data["td_for"] += match.get("td_for", 0)
                team_data["td_against"] += match.get("td_against", 0)
                team_data["tdd"] += match.get("tdd", 0)
                team_data["casualties_inflicted"] += match.get("casualties_inflicted", 0)
                team_data["casualties_sustained"] += match.get("casualties_sustained", 0)
                team_data["casualties_difference"] += match.get("casualties_difference", 0)
            team_data["tdd_casd"] = team_data.get("tdd", 0) + team_data.get("casualties_difference", 0)

            standing.append(team_data)

        standing = sorted(standing, key=lambda i: i["tdd_casd"], reverse=True)
        standing = sorted(standing, key=lambda i: i["win"], reverse=True)
        standing = sorted(standing, key=lambda i: i["points"], reverse=True)

        if len(standing) > 0:
            rank = 1
            for team in standing:
                team["rank"] = rank
                rank += 1
            competition_db.set_standing(EUROPEN_ID, 1, standing, "europen_standing", point_system)
        return standing, point_system

    @staticmethod
    def get_standing():
        competition_db = Competitions()
        cmp_standing = competition_db.get_standing(EUROPEN_ID, 1)

        if cmp_standing is None:
            standing, point_system = SpikeEurOpen.compute_standing()
        else:
            point_system = cmp_standing["point_system"]
            standing = cmp_standing["standing"]

        return standing, point_system, "europen_standing"
