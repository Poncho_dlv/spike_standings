#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

from spike_database.Competitions import Competitions
from spike_database.CustomCompetitionTeam import CustomCompetitionTeam
from spike_standings.SpikeStandings import SpikeStandings

spike_logger = logging.getLogger("spike_logger")

EUROPEN2_LEAGUE_ID = 79087
EUROPEN2_BIS_LEAGUE_ID = 84812
EUROPEN2_COMPETITION_ID = 180971
point_system = "2-1-0,INDPTS,H2H,TBR,CW3"
standing_type = "europen2_standing"
tie_breakers = ["coleman_waldorf_3", "external_tie_breaker", "head_to_head", "individual_points", "points"]  # Reversed version for sort


class SpikeEurOpen2:

    @staticmethod
    def compute_standing():
        competition_db = Competitions()
        standing = SpikeEurOpen2.compute_team_rank()
        competition_db.set_standing(EUROPEN2_COMPETITION_ID, 1, standing, standing_type, point_system)
        return standing, point_system

    @staticmethod
    def get_standing():
        competition_db = Competitions()
        cmp_standing = competition_db.get_standing(EUROPEN2_COMPETITION_ID, 1)

        if cmp_standing is None:
            standing, ps = SpikeEurOpen2.compute_team_rank()
        else:
            standing = cmp_standing["standing"]
        return standing, point_system, standing_type

    @staticmethod
    def compute_individual_rank():
        competition_db = Competitions()
        competitions_list = competition_db.get_competitions_in_league(EUROPEN2_LEAGUE_ID, 1)
        competitions_list.extend(competition_db.get_competitions_in_league(EUROPEN2_BIS_LEAGUE_ID, 1))
        standing_dict = {}
        for competition in competitions_list:
            if any(x in competition.get("name", "").lower() for x in ["r1t", "r2t", "r3t", "r4t", "r5t"]):
                standing, ps = SpikeStandings.compute_custom_ranking(competition, point_system)
                for team in standing:
                    if standing_dict.get(team.get("coach_id")) is None:
                        standing_dict[team.get("coach_id")] = team
                    else:
                        standing_dict[team.get("coach_id")]["points"] += team["points"]
                        standing_dict[team.get("coach_id")]["win"] += team["win"]
                        standing_dict[team.get("coach_id")]["draw"] += team["draw"]
                        standing_dict[team.get("coach_id")]["loss"] += team["loss"]
                        standing_dict[team.get("coach_id")]["games_played"] += team["games_played"]
                        standing_dict[team.get("coach_id")]["td_for"] += team["td_for"]
                        standing_dict[team.get("coach_id")]["td_against"] += team["td_against"]
                        standing_dict[team.get("coach_id")]["tdd"] += team["tdd"]
                        standing_dict[team.get("coach_id")]["casualties_inflicted"] += team["casualties_inflicted"]
                        standing_dict[team.get("coach_id")]["casualties_sustained"] += team["casualties_sustained"]
                        standing_dict[team.get("coach_id")]["casualties_difference"] += team["casualties_difference"]
                        standing_dict[team.get("coach_id")]["kill"] += team["kill"]
                        standing_dict[team.get("coach_id")]["coleman_waldorf"] += team["coleman_waldorf"]
                        standing_dict[team.get("coach_id")]["coleman_waldorf_2"] += team["coleman_waldorf_2"]
                        standing_dict[team.get("coach_id")]["head_to_head"].extend(team["head_to_head"])
                        standing_dict[team.get("coach_id")]["opponents"].extend(team["opponents"])
                        standing_dict[team.get("coach_id")]["matches"].extend(team["matches"])
                        standing_dict[team.get("coach_id")]["external_tie_breaker"] += team["external_tie_breaker"]
                        standing_dict[team.get("coach_id")]["coleman_waldorf_3"] += team["coleman_waldorf_3"]

        individual_standing = list(standing_dict.values())
        for tb in tie_breakers:
            if tb == "head_to_head":
                # Apply H2H on sorted rank by points
                individual_standing = sorted(individual_standing, key=lambda item: item.get("points", 0), reverse=True)
                size = len(individual_standing)
                for i in range(2):  # Hack for double sort, assuming that 3 coach tied by H2H are impossible to sort
                    for idx in range(1, size):
                        if individual_standing[idx - 1]["team_id"] in individual_standing[idx].get("head_to_head", []):
                            tmp = individual_standing[idx - 1]
                            individual_standing[idx - 1] = individual_standing[idx]
                            individual_standing[idx] = tmp
            else:
                individual_standing = sorted(individual_standing, key=lambda i: i.get(tb, 0), reverse=True)

        if len(individual_standing) > 0:
            rank = 1
            for team in individual_standing:
                team["rank"] = rank
                rank += 1

        return individual_standing

    @staticmethod
    def compute_team_rank():
        competition_db = Competitions()
        competitions_list = competition_db.get_competitions_in_league(EUROPEN2_LEAGUE_ID, 1)
        competitions_list.extend(competition_db.get_competitions_in_league(EUROPEN2_BIS_LEAGUE_ID, 1))
        team_db = CustomCompetitionTeam()
        teams_list = team_db.get_teams(EUROPEN2_COMPETITION_ID, 1)
        standing_dict = {}

        # init team data
        for team in teams_list:
            if standing_dict.get(team["_id"]) is None:
                standing_dict[team["_id"]] = team
                standing_dict[team["_id"]]["points"] = 0
                standing_dict[team["_id"]]["individual_points"] = 0
                standing_dict[team["_id"]]["individual_games"] = 0
                standing_dict[team["_id"]]["win"] = 0
                standing_dict[team["_id"]]["draw"] = 0
                standing_dict[team["_id"]]["loss"] = 0
                standing_dict[team["_id"]]["games_played"] = 0
                standing_dict[team["_id"]]["td_for"] = 0
                standing_dict[team["_id"]]["td_against"] = 0
                standing_dict[team["_id"]]["tdd"] = 0
                standing_dict[team["_id"]]["casualties_inflicted"] = 0
                standing_dict[team["_id"]]["casualties_sustained"] = 0
                standing_dict[team["_id"]]["casualties_difference"] = 0
                standing_dict[team["_id"]]["kill"] = 0
                standing_dict[team["_id"]]["coleman_waldorf"] = 0
                standing_dict[team["_id"]]["coleman_waldorf_2"] = 0
                standing_dict[team["_id"]]["head_to_head"] = []
                standing_dict[team["_id"]]["opponents"] = []
                standing_dict[team["_id"]]["matches"] = []
                standing_dict[team["_id"]]["external_tie_breaker"] = 0
                standing_dict[team["_id"]]["coleman_waldorf_3"] = 0

        for competition in competitions_list:
            if any(x in competition.get("name", "").lower() for x in ["r1t", "r2t", "r3t", "r4t", "r5t"]):
                standing, ps = SpikeStandings.compute_custom_ranking(competition, point_system)
                team_data = {}
                for team in standing:
                    parent_team = next((item for item in teams_list for member in item.get("members") if team.get("coach_id") == member.get("coach_id")), None)
                    if parent_team is not None:
                        if team_data.get(parent_team["_id"]) is None:
                            team_data[parent_team.get("_id")] = {}
                            team_data[parent_team.get("_id")]["_id"] = parent_team["_id"]
                            team_data[parent_team.get("_id")]["points"] = 0

                        team_data[parent_team.get("_id")]["points"] += team["points"]
                        standing_dict[parent_team.get("_id")]["individual_points"] += team["points"]
                        standing_dict[parent_team.get("_id")]["individual_games"] += team["games_played"]
                        standing_dict[parent_team.get("_id")]["td_for"] += team["td_for"]
                        standing_dict[parent_team.get("_id")]["td_against"] += team["td_against"]
                        standing_dict[parent_team.get("_id")]["tdd"] += team["tdd"]
                        standing_dict[parent_team.get("_id")]["casualties_inflicted"] += team["casualties_inflicted"]
                        standing_dict[parent_team.get("_id")]["casualties_sustained"] += team["casualties_sustained"]
                        standing_dict[parent_team.get("_id")]["casualties_difference"] += team["casualties_difference"]
                        standing_dict[parent_team.get("_id")]["kill"] += team["kill"]
                        standing_dict[parent_team.get("_id")]["coleman_waldorf"] += team["coleman_waldorf"]
                        standing_dict[parent_team.get("_id")]["coleman_waldorf_2"] += team["coleman_waldorf_2"]
                        standing_dict[parent_team.get("_id")]["external_tie_breaker"] += team["external_tie_breaker"]
                        standing_dict[parent_team.get("_id")]["coleman_waldorf_3"] += team["coleman_waldorf_3"]

                tmp_list = list(team_data.values())
                if len(tmp_list) == 2:
                    team_a = tmp_list[0]
                    team_b = tmp_list[1]

                    if team_a["points"] > team_b["points"]:
                        # Team A win
                        standing_dict[team_a["_id"]]["points"] += 2
                        standing_dict[team_a["_id"]]["win"] += 1
                        standing_dict[team_a["_id"]]["games_played"] += 1
                        standing_dict[team_a["_id"]]["head_to_head"].append(team_b["_id"])
                        standing_dict[team_a["_id"]]["opponents"].append(team_b["_id"])

                        standing_dict[team_b["_id"]]["loss"] += 1
                        standing_dict[team_b["_id"]]["games_played"] += 1
                        standing_dict[team_b["_id"]]["opponents"].append(team_a["_id"])
                    elif team_a["points"] < team_b["points"]:
                        # Team B win
                        standing_dict[team_b["_id"]]["points"] += 2
                        standing_dict[team_b["_id"]]["win"] += 1
                        standing_dict[team_b["_id"]]["games_played"] += 1
                        standing_dict[team_b["_id"]]["head_to_head"].append(team_a["_id"])
                        standing_dict[team_b["_id"]]["opponents"].append(team_a["_id"])

                        standing_dict[team_a["_id"]]["loss"] += 1
                        standing_dict[team_a["_id"]]["games_played"] += 1
                        standing_dict[team_a["_id"]]["opponents"].append(team_b["_id"])
                    else:
                        # Draw
                        standing_dict[team_a["_id"]]["points"] += 1
                        standing_dict[team_a["_id"]]["draw"] += 1
                        standing_dict[team_a["_id"]]["games_played"] += 1
                        standing_dict[team_a["_id"]]["opponents"].append(team_b["_id"])

                        standing_dict[team_b["_id"]]["points"] += 1
                        standing_dict[team_b["_id"]]["draw"] += 1
                        standing_dict[team_b["_id"]]["games_played"] += 1
                        standing_dict[team_b["_id"]]["opponents"].append(team_a["_id"])

        team_standing = list(standing_dict.values())
        for tb in tie_breakers:
            if tb == "head_to_head":
                # Apply H2H on sorted rank by points
                team_standing = sorted(team_standing, key=lambda item: item.get("points", 0), reverse=True)
                size = len(team_standing)
                for i in range(2):  # Hack for double sort, assuming that 3 coach tied by H2H are impossible to sort
                    for idx in range(1, size):
                        if team_standing[idx - 1]["_id"] in team_standing[idx].get("head_to_head", []):
                            tmp = team_standing[idx - 1]
                            team_standing[idx - 1] = team_standing[idx]
                            team_standing[idx] = tmp
            else:
                team_standing = sorted(team_standing, key=lambda item: item.get(tb, 0), reverse=True)

        if len(team_standing) > 0:
            rank = 1
            for team in team_standing:
                team["rank"] = rank
                rank += 1

        return team_standing
