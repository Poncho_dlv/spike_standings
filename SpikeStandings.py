#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math
import functools

from spike_database.Competitions import Competitions
from spike_database.Matches import Matches
from spike_database.ResourcesRequest import ResourcesRequest
from spike_model.Match import Match
from spike_standings import tie_breakers_data
from spike_utilities.Utilities import Utilities
from spike_requester.Utilities import Utilities as RequestUtils


class SpikeStandings:

    @staticmethod
    def compute_standing(competition_id: int, platform_id: int):
        competition_db = Competitions()
        competition_data = competition_db.get_competition_data(competition_id, platform_id)

        if competition_data.get("format") == "ladder" or competition_data.get("point_system") == "rank":
            point_system = "rank"
            standing = SpikeStandings.compute_standard_ranking(competition_data)
        elif competition_data.get("format") in ["round_robin", "swiss"]:
            standing, point_system = SpikeStandings.compute_custom_ranking(competition_data)
        else:
            point_system = None
            standing = []

        if len(standing) > 0:
            rank = 1
            for team in standing:
                team["rank"] = rank
                rank += 1
            competition_db.set_standing(competition_id, platform_id, standing, "spike_standing", point_system)
        return standing, point_system

    @staticmethod
    def build_team_result_from_dict(contests):
        teams_result = {}
        for contest in contests:
            match_list = contest.get("match_uuid", [])
            if len(match_list) > 0 and match_list[-1] is not None:
                match = RequestUtils.get_match(match_list[-1])
                home_team = contest.get("team_home", {})
                home_key = str(home_team.get("team_id"))
                if teams_result.get(home_key) is None:
                    teams_result[home_key] = {
                        "coach_name": home_team.get("coach_name"),
                        "coach_id": home_team.get("coach_id"),
                        "team_name":  home_team.get("team_name"),
                        "team_id": home_team.get("team_id"),
                        "logo": home_team.get("team_logo"),
                        "race": home_team.get("race"),
                        "points": 0,
                        "win": 0,
                        "draw": 0,
                        "loss": 0,
                        "games_played": 0,
                        "value": home_team.get("team_value"),
                        "td_for": 0,
                        "td_against": 0,
                        "tdd": 0,
                        "casualties_inflicted": 0,
                        "casualties_sustained": 0,
                        "casualties_difference": 0,
                        "kill": 0,
                        "coleman_waldorf": 0,
                        "coleman_waldorf_2": 0,
                        "head_to_head": [],
                        "opponents": [],
                        "matches": [],
                        "external_tie_breaker": 0,
                        "coleman_waldorf_3": 0
                    }

                away_team = contest.get("team_away", {})
                away_key = str(away_team.get("team_id"))
                if teams_result.get(away_key) is None:
                    teams_result[away_key] = {
                        "coach_name": away_team.get("coach_name"),
                        "coach_id": away_team.get("coach_id"),
                        "team_name":  away_team.get("team_name"),
                        "team_id": away_team.get("team_id"),
                        "logo": away_team.get("team_logo"),
                        "race": away_team.get("race"),
                        "points": 0,
                        "win": 0,
                        "draw": 0,
                        "loss": 0,
                        "games_played": 0,
                        "value": away_team.get("team_value"),
                        "td_for": 0,
                        "td_against": 0,
                        "tdd": 0,
                        "casualties_inflicted": 0,
                        "casualties_sustained": 0,
                        "casualties_difference": 0,
                        "kill": 0,
                        "coleman_waldorf": 0,
                        "coleman_waldorf_2": 0,
                        "head_to_head": [],
                        "opponents": [],
                        "matches": [],
                        "external_tie_breaker": 0,
                        "coleman_waldorf_3": 0
                    }

                if match.get_score_home() == match.get_score_away():
                    teams_result[home_key]["draw"] += 1
                    teams_result[away_key]["draw"] += 1
                elif match.get_score_home() > match.get_score_away():
                    teams_result[home_key]["win"] += 1
                    teams_result[away_key]["loss"] += 1
                    teams_result[home_key]["head_to_head"].append(away_team.get("team_id"))
                else:
                    teams_result[home_key]["loss"] += 1
                    teams_result[away_key]["win"] += 1
                    teams_result[away_key]["head_to_head"].append(home_team.get("team_id"))

                teams_result[home_key]["td_for"] += home_team.get("score")
                teams_result[home_key]["td_against"] += away_team.get("score")
                teams_result[home_key]["casualties_inflicted"] += match.get_inflicted_casualties_home()
                teams_result[home_key]["casualties_sustained"] += match.get_inflicted_casualties_away()
                teams_result[home_key]["kill"] += match.get_inflicted_dead_home()
                teams_result[home_key]["opponents"].append(away_team.get("team_id"))
                teams_result[home_key]["matches"].append(match.get_uuid())

                teams_result[away_key]["td_for"] += away_team.get("score")
                teams_result[away_key]["td_against"] += home_team.get("score")
                teams_result[away_key]["casualties_inflicted"] += match.get_inflicted_casualties_away()
                teams_result[away_key]["casualties_sustained"] += match.get_inflicted_casualties_home()
                teams_result[away_key]["kill"] += match.get_inflicted_dead_away()
                teams_result[away_key]["opponents"].append(home_team.get("team_id"))
                teams_result[away_key]["matches"].append(match.get_uuid())
        return teams_result

    @staticmethod
    def get_standing(competition_id: int, platform_id: int):
        competition_db = Competitions()
        cmp_standing = competition_db.get_standing(competition_id, platform_id)

        if cmp_standing is None or cmp_standing.get("standing_source") != "spike_standing":
            standing, point_system = SpikeStandings.compute_standing(competition_id, platform_id)
        else:
            point_system = cmp_standing["point_system"]
            standing = cmp_standing["standing"]
        return standing, point_system, "spike_standing"

    @staticmethod
    def compute_custom_ranking(competition_data: dict, point_system: str = None):
        standing = []
        contests = Utilities.get_contest(competition_data.get("id"), competition_data.get("platform_id"), "played", "spike_standing")
        if point_system is None:
            point_system = competition_data.get("point_system")
        tie_breakers = ["points"]

        if point_system is None:
            win_point = 3
            draw_point = 1
            loss_point = 0
            tie_breakers.extend(("tdd", "td_for"))
            point_system = "3-1-0,TDD,TD"
        else:
            tmp_pt_sys = point_system.split(",")
            win_point = int(tmp_pt_sys[0].split("-")[0].strip())
            draw_point = int(tmp_pt_sys[0].split("-")[1].strip())
            loss_point = int(tmp_pt_sys[0].split("-")[2].strip())
            tmp_pt_sys.remove(tmp_pt_sys[0])
            for entry in tmp_pt_sys:
                if entry in tie_breakers_data.keys():
                    tie_breakers.append(tie_breakers_data.get(entry.strip()))

        if len(contests) > 0:
            teams_result = SpikeStandings.build_team_result_from_dict(contests)

            for key, value in teams_result.items():
                if not competition_data.get("admin_standing", {}).get(key, {}).get("hide"):
                    value["win"] += competition_data.get("admin_standing", {}).get(key, {}).get("win", 0)
                    value["draw"] += competition_data.get("admin_standing", {}).get(key, {}).get("draw", 0)
                    value["loss"] += competition_data.get("admin_standing", {}).get(key, {}).get("loss", 0)
                    value["points"] += (value["win"] * win_point) + (value["draw"] * draw_point) + (value["loss"] * loss_point)
                    value["points"] += competition_data.get("admin_standing", {}).get(key, {}).get("point", 0)
                    value["games_played"] = value["win"] + value["draw"] + value["loss"]

                    value["td_for"] += competition_data.get("admin_standing", {}).get(key, {}).get("td_for", 0)
                    value["td_against"] += competition_data.get("admin_standing", {}).get(key, {}).get("td_against", 0)
                    value["tdd"] = value["td_for"] - value["td_against"]

                    value["casualties_inflicted"] += competition_data.get("admin_standing", {}).get(key, {}).get("casualties_inflicted", 0)
                    value["casualties_sustained"] += competition_data.get("admin_standing", {}).get(key, {}).get("casualties_sustained", 0)
                    value["casualties_difference"] = value["casualties_inflicted"] - value["casualties_sustained"]

                    value["coleman_waldorf"] = (value["casualties_inflicted"] / 2) + value["td_for"]
                    value["coleman_waldorf_2"] = (value["casualties_difference"] / 2) + value["tdd"]
                    value["coleman_waldorf_3"] = value["casualties_difference"] + value["tdd"]
                    value["external_tie_breaker"] = competition_data.get("admin_standing", {}).get(key, {}).get("external_tie_breaker", 0)

                    standing.append(value)

            def h2h_comparator(i, j):
                if i["team_id"] in j["head_to_head"]:
                    return -1
                if j["team_id"] in i["head_to_head"]:
                    return 1
                return 0

            keys = lambda i: [i.get(tb, 0) if tb != "head_to_head" else functools.cmp_to_key(h2h_comparator)(i) for tb in tie_breakers]
            standing = sorted(standing, key=keys, reverse=True)

        return standing, point_system

    @staticmethod
    def compute_standard_ranking(competition_data):
        standing = []
        matches_db = Matches()
        rsrc_db = ResourcesRequest()

        platform = rsrc_db.get_platform_label(competition_data.get("platform_id"))
        match_list = matches_db.get_matches_in_competition(competition_data.get("id"), platform)
        # formula https://forums.focus-home.com/topic/466/the-champions-ladder-ranking-formula
        cross_point = 0.2
        limit = 42
        a = 0.05
        target = 28
        x = math.log(a/(1-cross_point), 10) / math.log(1-(target/limit), 10)

        if len(match_list) > 0:
            teams_result = SpikeStandings.build_team_result_from_match(match_list)

            for key, value in teams_result.items():
                if not competition_data.get("admin_standing", {}).get(key, {}).get("hide"):
                    value["win"] += competition_data.get("admin_standing", {}).get(key, {}).get("win", 0)
                    value["draw"] += competition_data.get("admin_standing", {}).get(key, {}).get("draw", 0)
                    value["loss"] += competition_data.get("admin_standing", {}).get(key, {}).get("loss", 0)

                    value["games_played"] = value["win"] + value["draw"] + value["loss"]

                    value["td_for"] += competition_data.get("admin_standing", {}).get(key, {}).get("td_for", 0)
                    value["td_against"] += competition_data.get("admin_standing", {}).get(key, {}).get("td_against", 0)
                    value["tdd"] = value["td_for"] - value["td_against"]

                    value["casualties_inflicted"] += competition_data.get("admin_standing", {}).get(key, {}).get("casualties_inflicted", 0)
                    value["casualties_sustained"] += competition_data.get("admin_standing", {}).get(key, {}).get("casualties_sustained", 0)
                    value["casualties_difference"] = value["casualties_inflicted"] - value["casualties_sustained"]

                    value["coleman_waldorf"] = (value["casualties_inflicted"] / 2) + value["td_for"]
                    value["coleman_waldorf_2"] = (value["casualties_difference"] / 2) + value["tdd"]

                    win_pct = 100 * (value["win"] + value["draw"]/2)/value["games_played"]
                    rank_points = win_pct * (cross_point + (1 - cross_point) * (1 - ((1 - (0.5 * ((value["games_played"] + limit) - math.sqrt((value["games_played"] - limit) ** 2)) / limit)) ** x)))
                    rank_points += value["games_played"] * 0.02
                    value["sort"] = round(rank_points, 2)

                    standing.append(value)
            standing = sorted(standing, key=lambda i: i["sort"], reverse=True)

        return standing

    @staticmethod
    def build_team_result_from_match(matches):
        teams_result = {}
        for match in matches:
            uuid = match["uuid"]
            match = Match(match)
            match.set_uuid(uuid)
            home_key = str(match.get_team_home().get_id())
            if teams_result.get(home_key) is None:
                teams_result[home_key] = {
                    "coach_name": match.get_coach_home().get_name(),
                    "coach_id": match.get_coach_home().get_id(),
                    "team_name": match.get_team_home().get_name(),
                    "team_id": match.get_team_home().get_id(),
                    "logo": match.get_team_home().get_logo(),
                    "race": match.get_team_home().get_race(),
                    "points": 0,
                    "win": 0,
                    "draw": 0,
                    "loss": 0,
                    "games_played": 0,
                    "value": match.get_team_home().get_team_value(),
                    "td_for": 0,
                    "td_against": 0,
                    "tdd": 0,
                    "casualties_inflicted": 0,
                    "casualties_sustained": 0,
                    "casualties_difference": 0,
                    "kill": 0,
                    "coleman_waldorf": 0,
                    "coleman_waldorf_2": 0,
                    "head_to_head": []
                }

            away_key = str(match.get_team_away().get_id())
            if teams_result.get(away_key) is None:
                teams_result[away_key] = {
                    "coach_name": match.get_coach_away().get_name(),
                    "coach_id": match.get_coach_away().get_id(),
                    "team_name": match.get_team_away().get_name(),
                    "team_id": match.get_team_away().get_id(),
                    "logo": match.get_team_away().get_logo(),
                    "race": match.get_team_away().get_race(),
                    "points": 0,
                    "win": 0,
                    "draw": 0,
                    "loss": 0,
                    "games_played": 0,
                    "value": match.get_team_away().get_team_value(),
                    "td_for": 0,
                    "td_against": 0,
                    "tdd": 0,
                    "casualties_inflicted": 0,
                    "casualties_sustained": 0,
                    "casualties_difference": 0,
                    "kill": 0,
                    "coleman_waldorf": 0,
                    "coleman_waldorf_2": 0,
                    "head_to_head": []
                }

            if match.get_score_home() == match.get_score_away():
                teams_result[home_key]["draw"] += 1
                teams_result[away_key]["draw"] += 1
            elif match.get_score_home() > match.get_score_away():
                teams_result[home_key]["win"] += 1
                teams_result[away_key]["loss"] += 1
            else:
                teams_result[home_key]["loss"] += 1
                teams_result[away_key]["win"] += 1

            teams_result[home_key]["td_for"] += match.get_score_home()
            teams_result[home_key]["td_against"] += match.get_score_away()
            teams_result[home_key]["casualties_inflicted"] += match.get_inflicted_casualties_home()
            teams_result[home_key]["casualties_sustained"] += match.get_inflicted_casualties_away()
            teams_result[home_key]["kill"] += match.get_inflicted_dead_home()

            teams_result[away_key]["td_for"] += match.get_score_away()
            teams_result[away_key]["td_against"] += match.get_score_home()
            teams_result[away_key]["casualties_inflicted"] += match.get_inflicted_casualties_away()
            teams_result[away_key]["casualties_sustained"] += match.get_inflicted_casualties_home()
            teams_result[away_key]["kill"] += match.get_inflicted_dead_away()
        return teams_result
