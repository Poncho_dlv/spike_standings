#!/usr/bin/env python
# -*- coding: utf-8 -*-

from spike_database.Competitions import Competitions
from spike_standings import tie_breakers_data
from spike_utilities.Utilities import Utilities
from spike_requester.Utilities import Utilities as RequestUtils


class Spikeoween:

    @staticmethod
    def compute_standing(competition_id: int, platform_id: int):
        standing = []
        competition_db = Competitions()
        competition_data = competition_db.get_competition_data(competition_id, platform_id)

        contests = Utilities.get_contest(competition_id, platform_id, "played")

        point_system = competition_data.get("point_system")
        tie_breakers = ["points"]

        if point_system is None:
            win_point = 3
            draw_point = 1
            loss_point = 0
            tie_breakers.extend(("tdd", "CASD"))
            point_system = "3-1-0,TDD,CASD"
        else:
            tmp_pt_sys = point_system.split(",")
            win_point = int(tmp_pt_sys[0].split("-")[0].strip())
            draw_point = int(tmp_pt_sys[0].split("-")[1].strip())
            loss_point = int(tmp_pt_sys[0].split("-")[2].strip())
            tmp_pt_sys.remove(tmp_pt_sys[0])
            for entry in tmp_pt_sys:
                tie_breakers.append(tie_breakers_data.get(entry.strip()))

        if len(contests) > 0:
            teams_result = Spikeoween.build_team_result_from_dict(contests)

            for key, value in teams_result.items():
                if not competition_data.get("admin_standing", {}).get(key, {}).get("hide"):
                    value["win"] += competition_data.get("admin_standing", {}).get(key, {}).get("win", 0)
                    value["draw"] += competition_data.get("admin_standing", {}).get(key, {}).get("draw", 0)
                    value["loss"] += competition_data.get("admin_standing", {}).get(key, {}).get("loss", 0)

                    value["games_played"] = value["win"] + value["draw"] + value["loss"]

                    value["td_for"] += competition_data.get("admin_standing", {}).get(key, {}).get("td_for", 0)
                    value["td_against"] += competition_data.get("admin_standing", {}).get(key, {}).get("td_against", 0)
                    value["tdd"] = value["td_for"] - value["td_against"]

                    value["casualties_inflicted"] += competition_data.get("admin_standing", {}).get(key, {}).get("casualties_inflicted", 0)
                    value["casualties_sustained"] += competition_data.get("admin_standing", {}).get(key, {}).get("casualties_sustained", 0)
                    value["casualties_difference"] = value["casualties_inflicted"] - value["casualties_sustained"]

                    value["coleman_waldorf"] = (value["casualties_inflicted"] / 2) + value["td_for"]

                    value["points"] += (value["win"] * win_point) + (value["draw"] * draw_point) + (value["loss"] * loss_point) + competition_data.get("admin_standing", {}).get(key, {}).get("point", 0)
                    value["points"] += (value["td_for"] * 5) + value["kill"] + (value["casualties_inflicted"] * 2)
                    standing.append(value)

            tie_breakers.reverse()
            for tb in tie_breakers:
                standing = sorted(standing, key=lambda i: i[tb], reverse=True)

            rank = 1
            for team in standing:
                team["rank"] = rank
                rank += 1
            competition_db.set_standing(competition_id, platform_id, standing, "spikoween", point_system)
        return standing, point_system

    @staticmethod
    def build_team_result_from_dict(contests):
        teams_result = {}
        for contest in contests:
            match_list = contest.get("match_uuid", [])
            if len(match_list) > 0:
                match = RequestUtils.get_match(match_list[-1])
                home_team = contest.get("team_home", {})
                home_key = str(home_team.get("team_id"))
                if teams_result.get(home_key) is None:
                    teams_result[home_key] = {
                        "coach_name": home_team.get("coach_name"),
                        "coach_id": home_team.get("coach_id"),
                        "team_name":  home_team.get("team_name"),
                        "team_id": home_team.get("team_id"),
                        "logo": home_team.get("team_logo"),
                        "race": home_team.get("race"),
                        "points": 0,
                        "win": 0,
                        "draw": 0,
                        "loss": 0,
                        "games_played": 0,
                        "value": home_team.get("team_value"),
                        "td_for": 0,
                        "td_against": 0,
                        "tdd": 0,
                        "casualties_inflicted": 0,
                        "casualties_sustained": 0,
                        "casualties_difference": 0,
                        "kill": 0,
                        "coleman_waldorf": 0,
                        "head_to_head": 0
                    }

                away_team = contest.get("team_away", {})
                away_key = str(away_team.get("team_id"))
                if teams_result.get(away_key) is None:
                    teams_result[away_key] = {
                        "coach_name": away_team.get("coach_name"),
                        "coach_id": away_team.get("coach_id"),
                        "team_name":  away_team.get("team_name"),
                        "team_id": away_team.get("team_id"),
                        "logo": away_team.get("team_logo"),
                        "race": away_team.get("race"),
                        "points": 0,
                        "win": 0,
                        "draw": 0,
                        "loss": 0,
                        "games_played": 0,
                        "value": away_team.get("team_value"),
                        "td_for": 0,
                        "td_against": 0,
                        "tdd": 0,
                        "casualties_inflicted": 0,
                        "casualties_sustained": 0,
                        "casualties_difference": 0,
                        "kill": 0,
                        "coleman_waldorf": 0,
                        "head_to_head": 0
                    }

                winner = contest.get("winner")
                if winner is None:
                    teams_result[home_key]["draw"] += 1
                    teams_result[away_key]["draw"] += 1
                elif winner.get("coach_id") == home_team.get("coach_id"):
                    teams_result[home_key]["win"] += 1
                    teams_result[away_key]["loss"] += 1
                else:
                    teams_result[home_key]["loss"] += 1
                    teams_result[away_key]["win"] += 1

                teams_result[home_key]["td_for"] += home_team.get("score")
                teams_result[home_key]["td_against"] += away_team.get("score")
                teams_result[home_key]["casualties_inflicted"] += match.get_inflicted_casualties_home()
                teams_result[home_key]["casualties_sustained"] += match.get_inflicted_casualties_away()
                teams_result[home_key]["kill"] += match.get_inflicted_dead_home()

                teams_result[away_key]["td_for"] += away_team.get("score")
                teams_result[away_key]["td_against"] += home_team.get("score")
                teams_result[away_key]["casualties_inflicted"] += match.get_inflicted_casualties_away()
                teams_result[away_key]["casualties_sustained"] += match.get_inflicted_casualties_away()
                teams_result[away_key]["kill"] += match.get_inflicted_dead_away()
        return teams_result

    @staticmethod
    def get_standing(competition_id: int, platform_id: int):
        competition_db = Competitions()
        cmp_standing = competition_db.get_standing(competition_id, platform_id)

        if cmp_standing is None:
            standing, point_system = Spikeoween.compute_standing(competition_id, platform_id)
        else:
            point_system = cmp_standing["point_system"]
            standing = cmp_standing["standing"]
        return standing, point_system, "spikoween"
