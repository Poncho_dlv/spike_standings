#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math

from spike_database.Competitions import Competitions
from spike_database.ImpactPlayers import ImpactPlayers
from spike_database.Matches import Matches
from spike_database.ResourcesRequest import ResourcesRequest
from spike_model.Match import Match
from spike_standings.BloodBowl2Standings import BloodBowl2Standing
from spike_standings.SpikeEurOpen import SpikeEurOpen, EUROPEN_ID
from spike_standings.SpikeEurOpen2 import SpikeEurOpen2, EUROPEN2_LEAGUE_ID, EUROPEN2_BIS_LEAGUE_ID
from spike_standings.SpikeStandings import SpikeStandings
from spike_standings.Spikoween import Spikeoween
from spike_standings.VDGStanding import VDGStanding, VDG_LEAGUE_ID
from spike_utilities.Utilities import Utilities as SpikeUtilities


class Utilities:

    @staticmethod
    def get_standing(competition_id: int, platform_id: int):
        competition_db = Competitions()
        competition_data = competition_db.get_competition_data(competition_id, platform_id)
        standing_source = competition_data.get("standing_source")

        if competition_data.get("format") != "single_elimination":
            if SpikeUtilities.is_cabal_vision(competition_data.get("league_id"), platform_id):
                return competition_data.get("standing")[:100], "rank", "blood_bowl_2_standing"

            if competition_data.get("league_id") in [EUROPEN2_LEAGUE_ID, EUROPEN2_BIS_LEAGUE_ID] and platform_id == 1:
                return SpikeEurOpen2.get_standing()

            if competition_id == EUROPEN_ID and platform_id == 1:
                return SpikeEurOpen.get_standing()

            if competition_data.get("contest_fully_collected", False) and standing_source == "goblin_spy_standing":
                # In that case we recompute standing
                SpikeStandings.compute_standing(competition_id, platform_id)
                return SpikeStandings.get_standing(competition_id, platform_id)

            if standing_source == "spikoween":
                return Spikeoween.get_standing(competition_id, platform_id)

            if standing_source == "blood_bowl_2_standing":
                return BloodBowl2Standing.get_standing(competition_id, platform_id)

            if competition_data.get("contest_fully_collected", False):
                return SpikeStandings.get_standing(competition_id, platform_id)

            # Spike standing as default ranking
            return SpikeStandings.get_standing(competition_id, platform_id)
        else:
            return []

    @staticmethod
    def compute_standing(competition_id: int, platform_id: int):
        competition_db = Competitions()
        competition_data = competition_db.get_competition_data(competition_id, platform_id)
        standing_source = competition_data.get("standing_source")

        if competition_data.get("format") != "single_elimination":
            if competition_data.get("league_id") in [EUROPEN2_LEAGUE_ID, EUROPEN2_BIS_LEAGUE_ID] and platform_id == 1:
                SpikeEurOpen2.compute_standing()
            elif competition_id == 158165 and platform_id == 1:  # Europen
                SpikeEurOpen.compute_standing()
            elif standing_source == "spikoween":
                Spikeoween.compute_standing(competition_id, platform_id)
            elif competition_data.get("contest_fully_collected", False):
                SpikeStandings.compute_standing(competition_id, platform_id)
            #elif standing_source == "goblin_spy_standing" or SpikeUtilities.is_cabal_vision(competition_data.get("league_id"), platform_id):
                #GoblinSpyStandings.compute_standing(competition_id, platform_id)
            elif standing_source == "blood_bowl_2_standing":
                BloodBowl2Standing.compute_standing(competition_id, platform_id)
            else:
                SpikeStandings.compute_standing(competition_id, platform_id)

            if competition_data.get("league_id") == VDG_LEAGUE_ID:
                VDGStanding.compute_standing()

    @staticmethod
    def get_impact_player_ranking(competition_id: int, platform_id: int):
        impact_player_db = ImpactPlayers()
        ranked_player = impact_player_db.get_impact_players(competition_id, platform_id)
        if ranked_player:
            return ranked_player
        return Utilities.compute_impact_players_ranking(competition_id, platform_id)

    @staticmethod
    def compute_impact_players_ranking(competition_id: int, platform_id: int):
        match_db = Matches()
        impact_player_db = ImpactPlayers()
        rsrc_db = ResourcesRequest()
        impact_players = {}
        ranked_player = []

        platform = rsrc_db.get_platform_label(platform_id)
        match_list = match_db.get_matches_in_competition(competition_id, platform)

        for match in match_list:
            match = Match(match)
            for player in match.get_team_home().get_players():
                if not player.is_champion() and player.get_id():
                    if not impact_players.get(str(player.get_id())):
                        impact_players[str(player.get_id())] = {
                            "name": player.get_name(),
                            "id": player.get_id(),
                            "race": match.get_team_home().get_race(),
                            "type": player.get_type("en"),
                            "team_name": match.get_team_home().get_name(),
                            "team_id": match.get_team_home().get_id(),
                            "coach_name": match.get_team_home().get_coach().get_name(),
                            "coach_id": match.get_team_home().get_coach().get_id(),
                            "team_logo": match.get_team_home().get_logo(),
                            "points": 0,
                            "data": {
                                "Blocks": 0,
                                "Armour Breaks": 0,
                                "Kos": 0,
                                "Casualties": 0,
                                "Kills": 0,
                                "Surfs": 0,
                                "Passes": 0,
                                "Passes Dist": 0,
                                "Catches": 0,
                                "Interceptions": 0,
                                "Carrying": 0,
                                "Touchdowns": 0
                            }
                        }
                    impact_players[str(player.get_id())]["data"]["Blocks"] += player.get_inflicted_Block()
                    impact_players[str(player.get_id())]["data"]["Armour Breaks"] += player.get_inflicted_AvBr()
                    impact_players[str(player.get_id())]["data"]["Kos"] += player.get_inflicted_Ko()
                    impact_players[str(player.get_id())]["data"]["Casualties"] += player.get_inflicted_casualties()
                    impact_players[str(player.get_id())]["data"]["Kills"] += player.get_inflicted_dead()
                    impact_players[str(player.get_id())]["data"]["Surfs"] += player.get_inflicted_surfs()
                    impact_players[str(player.get_id())]["data"]["Passes"] += player.get_nb_passes()
                    impact_players[str(player.get_id())]["data"]["Passes Dist"] += player.get_passes_distance()
                    impact_players[str(player.get_id())]["data"]["Catches"] += player.get_nb_catches()
                    impact_players[str(player.get_id())]["data"]["Interceptions"] += player.get_nb_interceptions()
                    impact_players[str(player.get_id())]["data"]["Carrying"] += player.get_carrying_distance()
                    impact_players[str(player.get_id())]["data"]["Touchdowns"] += player.get_nb_touchdowns()

            for player in match.get_team_away().get_players():
                if not player.is_champion() and player.get_id():
                    if not impact_players.get(str(player.get_id())):
                        impact_players[str(player.get_id())] = {
                            "name": player.get_name(),
                            "id": player.get_id(),
                            "race": match.get_team_away().get_race(),
                            "type": player.get_type("en"),
                            "team_name": match.get_team_away().get_name(),
                            "team_id": match.get_team_away().get_id(),
                            "coach_name": match.get_team_away().get_coach().get_name(),
                            "coach_id": match.get_team_away().get_coach().get_id(),
                            "team_logo": match.get_team_away().get_logo(),
                            "points": 0,
                            "data": {
                                "Blocks": 0,
                                "Armour Breaks": 0,
                                "Kos": 0,
                                "Casualties": 0,
                                "Kills": 0,
                                "Surfs": 0,
                                "Passes": 0,
                                "Passes Dist": 0,
                                "Catches": 0,
                                "Interceptions": 0,
                                "Carrying": 0,
                                "Touchdowns": 0
                            }
                        }
                    impact_players[str(player.get_id())]["data"]["Blocks"] += player.get_inflicted_Block()
                    impact_players[str(player.get_id())]["data"]["Armour Breaks"] += player.get_inflicted_AvBr()
                    impact_players[str(player.get_id())]["data"]["Kos"] += player.get_inflicted_Ko()
                    impact_players[str(player.get_id())]["data"]["Casualties"] += player.get_inflicted_casualties()
                    impact_players[str(player.get_id())]["data"]["Kills"] += player.get_inflicted_dead()
                    impact_players[str(player.get_id())]["data"]["Surfs"] += player.get_inflicted_surfs()
                    impact_players[str(player.get_id())]["data"]["Passes"] += player.get_nb_passes()
                    impact_players[str(player.get_id())]["data"]["Passes Dist"] += player.get_passes_distance()
                    impact_players[str(player.get_id())]["data"]["Catches"] += player.get_nb_catches()
                    impact_players[str(player.get_id())]["data"]["Interceptions"] += player.get_nb_interceptions()
                    impact_players[str(player.get_id())]["data"]["Carrying"] += player.get_carrying_distance()
                    impact_players[str(player.get_id())]["data"]["Touchdowns"] += player.get_nb_touchdowns()

        for player in impact_players.values():
            player["points"] = math.ceil(player["data"]["Blocks"] / 5.)
            player["points"] += math.ceil(player["data"]["Armour Breaks"] / 3.)
            player["points"] += player["data"]["Kos"]
            player["points"] += player["data"]["Casualties"]
            player["points"] += player["data"]["Kills"] * 2
            player["points"] += player["data"]["Surfs"] * 2
            player["points"] += player["data"]["Passes"] * 2
            player["points"] += math.ceil(player["data"]["Passes Dist"] / 20.)
            player["points"] += player["data"]["Catches"] * 2
            player["points"] += player["data"]["Interceptions"] * 5
            player["points"] += math.ceil(player["data"]["Carrying"] / 50.)
            player["points"] += player["data"]["Touchdowns"] * 5

            ranked_player.append(player)
        ranked_player = sorted(ranked_player, key=lambda i: i['points'], reverse=True)
        impact_player_db.set_impact_players(competition_id, platform_id, ranked_player[:100])
        return ranked_player
