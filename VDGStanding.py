#!/usr/bin/env python
# -*- coding: utf-8 -*-

from spike_database.Competitions import Competitions
from spike_database.Leagues import Leagues

VDG_LEAGUE_ID = 82110
PLATFORM_ID = 1


class VDGStanding:

    @staticmethod
    def compute_standing():
        league_db = Leagues()
        competition_db = Competitions()
        coaches_result = {}

        league_data = league_db.get_league_data(VDG_LEAGUE_ID, PLATFORM_ID)
        cmp_list = []

        def populate_cmp_list(tier: str):
            cmp_list_key = "{}_list".format(tier)
            cmp_coef_key = "{}_coefficient".format(tier)
            for cmp in league_data.get(cmp_list_key, []):
                cmp_list.append({"id": cmp.get("id"), "point_weight": league_data.get(cmp_coef_key, 1)})

        populate_cmp_list("t1")
        populate_cmp_list("t2")
        populate_cmp_list("t3")
        populate_cmp_list("t4")

        for competition in cmp_list:
            cmp_standing = competition_db.get_standing(competition.get("id"), PLATFORM_ID)
            if cmp_standing is not None:
                for team in cmp_standing.get("standing", []):
                    if coaches_result.get(team.get("coach_id")) is None:
                        coaches_result[team.get("coach_id")] = {
                            "coach_id": team.get("coach_id"),
                            "coach_name": team.get("coach_name"),
                            "points": 0,
                            "win": 0,
                            "draw": 0,
                            "loss": 0,
                            "games_played": 0
                        }

                    coaches_result[team.get("coach_id")]["points"] += team.get("points", 0) * competition.get("point_weight")
                    coaches_result[team.get("coach_id")]["win"] += team.get("win", 0)
                    coaches_result[team.get("coach_id")]["draw"] += team.get("draw", 0)
                    coaches_result[team.get("coach_id")]["loss"] += team.get("loss", 0)
                    coaches_result[team.get("coach_id")]["games_played"] += team.get("games_played", 0)

        standing = list(coaches_result.values())
        standing = sorted(standing, key=lambda i: i["points"], reverse=True)
        if len(standing) > 0:
            rank = 1
            for team in standing:
                team["rank"] = rank
                rank += 1
            league_db.set_custom_data(VDG_LEAGUE_ID, PLATFORM_ID, {"standing": standing})
        return standing, None

    @staticmethod
    def get_standing():
        league_db = Leagues()
        league_data = league_db.get_league_data(VDG_LEAGUE_ID, PLATFORM_ID)

        if league_data is None or league_data.get("standing") is None:
            standing, point_system = VDGStanding.compute_standing()
        else:
            standing = league_data["standing"]
        return standing, None, None
