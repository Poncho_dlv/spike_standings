#!/usr/bin/env python
# -*- coding: utf-8 -*-

tie_breaker = [
    ("", ""),
    ("TD", "Touchdowns"),
    ("TDD", "Touchdown difference"),
    ("CAS", "Casualties"),
    ("CASD", "Casualties difference"),
    ("KILL", "Number of kills"),
    ("CW", "Coleman Waldorf"),
    ("CW2", "Coleman Waldorf 2"),
    ("CW3", "Coleman Waldorf 3"),
    ("WIN", "Number of wins"),
    ("H2H", "Head to Head"),
    ("TBR", "External tie breaker")
]

tie_breakers_data = {
    "TD": "td_for",
    "TDD": "tdd",
    "CAS": "casualties_inflicted",
    "CASD": "casualties_difference",
    "KILL": "kill",
    "CW": "coleman_waldorf",
    "CW2": "coleman_waldorf_2",
    "CW3": "coleman_waldorf_3",
    "WIN": "win",
    "H2H": "head_to_head",
    "TBR": "external_tie_breaker"
}

standing_source = [
    ("spike_standing", "Spike"),
    ("blood_bowl_2_standing", "Blood Bowl 2")
]

point_system = [
    ("points", "Custom"),
    ("rank", "Rank")
]
